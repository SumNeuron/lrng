import os

'''-----------------------------------------------------------------------------
DIRECTORIES AND FILES
-----------------------------------------------------------------------------'''
MODULE_DIR      = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR     = os.path.join(MODULE_DIR, '..')

JUYPTER_DIR     = os.path.join(PROJECT_DIR, 'juypter')
