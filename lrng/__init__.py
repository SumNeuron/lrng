from .lrng import LabeledRange, LabeledRanges
from .numba import coalesce, label_range, relevant_labels
name = 'lrng'

major = 1
minor = 0
patch = 1

version = '{}.{}.{}'.format(major, minor, patch)
description = 'Labeled Ranges'
