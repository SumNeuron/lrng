# lrng

A simple utility for labeled ranges
Click [here](https://sumneuron.gitlab.io/lrng/lrng.html) for documentation.
For use cases consult the [jupyter notebooks](https://gitlab.com/SumNeuron/lrng/-/tree/master/jupyter).